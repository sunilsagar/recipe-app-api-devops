terraform {
    backend "s3" {
        bucket = "recipe-app-api-devops-tfstate-sunil"
        key = "recipe-app.tfstate"
        region = "ap-southeast-1"
        encrypt = true
        dynamodb_table = "recipe-app-api-devops-tf-state-lock"
    }
}

provider "aws" {
    region = "ap-southeast-1"
    version = "~> 2.54.0"
}